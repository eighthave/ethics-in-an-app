---
title: Ethics in an App - Glossary
subtitle: Glossary
---

# smartphone 
  
[https://en.wikipedia.org/wiki/Smartphone](https://en.wikipedia.org/wiki/Smartphone)

# desktop computers 
  
[https://en.wikipedia.org/wiki/Desktop_computer](https://en.wikipedia.org/wiki/Desktop_computer)

# laptop 
  
[https://en.wikipedia.org/wiki/Laptop](https://en.wikipedia.org/wiki/Laptop)

# App (mobile App) 
  
[https://en.wikipedia.org/wiki/Mobile_app](https://en.wikipedia.org/wiki/Mobile_app)

# interface 
  
[https://en.wikipedia.org/wiki/Interface_(computing)](https://en.wikipedia.org/wiki/Interface_(computing))

# Digital Humanism 
  
[https://dighum.ec.tuwien.ac.at/](https://dighum.ec.tuwien.ac.at/)

# App stores 
  
[https://en.wikipedia.org/wiki/App_store](https://en.wikipedia.org/wiki/App_store)

# outsourcing the workload 

The term "outsourcing" refers to a strategy whereby corporate tasks and structures are delgated to external contractors. This can be on the level of individual tasks, specific tasks, or entire processes.

# user
  
[https://en.wikipedia.org/wiki/User_(computing)](https://en.wikipedia.org/wiki/User_(computing))

# user-experience 
  
[https://en.wikipedia.org/wiki/User_experience](https://en.wikipedia.org/wiki/User_experience)

# user profiling 
  
[https://en.wikipedia.org/wiki/User_profile](https://en.wikipedia.org/wiki/User_profile)

We refer to the automated strategy of building multiple profiles and combining them.

# GDPR 
  
[https://en.wikipedia.org/wiki/General_Data_Protection_Regulation](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)

# sensors 
  
[https://en.wikipedia.org/wiki/Sensor](https://en.wikipedia.org/wiki/Sensor)

# surveillance technologies 
  
[https://en.wikipedia.org/wiki/Mass_surveillance_industry](https://en.wikipedia.org/wiki/Mass_surveillance_industry)

# regulation 
  
[https://en.wikipedia.org/wiki/Regulation](https://en.wikipedia.org/wiki/Regulation)

# operating system 
  
[https://en.wikipedia.org/wiki/Operating_system](https://en.wikipedia.org/wiki/Operating_system)

# privacy by design- 
  
[https://en.wikipedia.org/wiki/Privacy_by_design](https://en.wikipedia.org/wiki/Privacy_by_design)

# obfuscation 
  
[https://en.wikipedia.org/wiki/Data_masking](https://en.wikipedia.org/wiki/Data_masking)

# fine-granular 
  
[https://en.wikipedia.org/wiki/Lock_(computer_science)](https://en.wikipedia.org/wiki/Lock_(computer_science))

# personal identifiable information 
  
[https://en.wikipedia.org/wiki/Gathering_of_personally_identifiable_information](https://en.wikipedia.org/wiki/Gathering_of_personally_identifiable_information)

# informed consent 
  
[https://usercentrics.com/knowledge-hub/obtaining-user-consent-these-five-tricks-are-not-gdpr-compliant/](https://usercentrics.com/knowledge-hub/obtaining-user-consent-these-five-tricks-are-not-gdpr-compliant/)

# data points 
  
[https://en.wikipedia.org/wiki/Unit_of_observation#Data_point](https://en.wikipedia.org/wiki/Unit_of_observation#Data_point)

# pre-installed Apps 
  
- [https://techcult.com/how-to-delete-pre-installed-apps-on-android/](https://techcult.com/how-to-delete-pre-installed-apps-on-android/)
- [https://en.wikipedia.org/wiki/Pre-installed_iOS_apps](https://en.wikipedia.org/wiki/Pre-installed_iOS_apps)

# cloud services 
  
[https://en.wikipedia.org/wiki/Cloud_computing](https://en.wikipedia.org/wiki/Cloud_computing)

# side-loading 
  
[https://helpdeskgeek.com/help-desk/what-is-sideloading/](https://helpdeskgeek.com/help-desk/what-is-sideloading/)

# disclaimers 
  
[https://en.wikipedia.org/wiki/Disclaimer](https://en.wikipedia.org/wiki/Disclaimer)

# nudging 
  
[https://uxdesign.cc/persuasive-design-nudging-users-in-the-right-direction-5af4a6f8c06f](https://uxdesign.cc/persuasive-design-nudging-users-in-the-right-direction-5af4a6f8c06f)

# opt-in 
  
[https://termly.io/resources/articles/opt-in-vs-opt-out/](https://termly.io/resources/articles/opt-in-vs-opt-out/)

# simple terms 
  
[https://www.plainlanguage.gov/guidelines/words/use-simple-words-phrases/](https://www.plainlanguage.gov/guidelines/words/use-simple-words-phrases/)

# geoblocking 
  
[https://en.wikipedia.org/wiki/Geo-blocking](https://en.wikipedia.org/wiki/Geo-blocking)

# IP address 
  
[https://en.wikipedia.org/wiki/IP_address](https://en.wikipedia.org/wiki/IP_address)

# vertical integration 
  
[https://en.wikipedia.org/wiki/Vertical_integration](https://en.wikipedia.org/wiki/Vertical_integration)

# monopoly 
  
[https://en.wikipedia.org/wiki/Monopoly](https://en.wikipedia.org/wiki/Monopoly)

# Digital Markets Act 
  
[https://en.wikipedia.org/wiki/Digital_Markets_Act](https://en.wikipedia.org/wiki/Digital_Markets_Act)

# obsolescence 
  
[https://en.wikipedia.org/wiki/Obsolescence](https://en.wikipedia.org/wiki/Obsolescence)

# e-waste 
  
[https://en.wikipedia.org/wiki/Electronic_waste](https://en.wikipedia.org/wiki/Electronic_waste)

# archivability 
  
[https://library.stanford.edu/projects/web-archiving/archivability](https://library.stanford.edu/projects/web-archiving/archivability)

# public code 
  
[https://en.wikipedia.org/wiki/Public_domain](https://en.wikipedia.org/wiki/Public_domain)

# licensing 
  
[https://snyk.io/learn/what-is-a-software-license/](https://snyk.io/learn/what-is-a-software-license/)

# lock-in effects 
  
[https://en.wikipedia.org/wiki/Vendor_lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in)

# F-Droid 
  
[https://f-droid.org/](https://f-droid.org/)

# proprietary libraries (Google Play Services). 
  
[https://en.wikipedia.org/wiki/Proprietary_software](https://en.wikipedia.org/wiki/Proprietary_software)

# participatory decision making 
  
[https://en.wikipedia.org/wiki/Participative_decision-making_in_organizations](https://en.wikipedia.org/wiki/Participative_decision-making_in_organizations)

# use-case 
  
[https://en.wikipedia.org/wiki/Use_case](https://en.wikipedia.org/wiki/Use_case)

# bottom-up 
  
[https://en.wikipedia.org/wiki/Top-down_and_bottom-up_design](https://en.wikipedia.org/wiki/Top-down_and_bottom-up_design)

# Open Data 
  
[https://en.wikipedia.org/wiki/Open_data](https://en.wikipedia.org/wiki/Open_data)

# decentralized 
  
[https://en.wikipedia.org/wiki/Decentralization](https://en.wikipedia.org/wiki/Decentralization)

# protocol based money 
  
[https://medium.com/genesishack/draft-what-are-blockchain-protocols-and-how-do-they-work-94815be5efa7](https://medium.com/genesishack/draft-what-are-blockchain-protocols-and-how-do-they-work-94815be5efa7)

# micro credits
  
[https://en.wikipedia.org/wiki/Microcredit](https://en.wikipedia.org/wiki/Microcredit)

# in-App purchases 

[https://www.investopedia.com/terms/i/inapp-purchasing.asp](https://www.investopedia.com/terms/i/inapp-purchasing.asp)