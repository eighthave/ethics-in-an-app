---
title: Ethics in an App - About
subtitle: About
showlogos: true
---

**Hans-Christoph Steiner** works to make private-by-design easy for all. He spends his time making private software as usable as possible; designing interactions focused on perceptual capabilities; and composing music with computers. With an emphasis on collaboration, he works on free software for mobile and embedded devices, free wireless networks that build community, musical robots that listen, programming environments to let you play with math, and jet-powered things that you can ride.

**Aaron Kaplan** is a creative IT (security) person, founder of the free wireless mesh community funkfeuer.at and bettercrypto.org, frequently speaking at IT security conferences. Member of the board of directors of FIRST.org between 2014-2018. He rethinks digital society as a whole and is the initiator of Ethics-in-an-App together with Hans-Christoph Steiner. @ [http://www.lo-res.org/~aaron/](http://www.lo-res.org/~aaron/)

**Thomas Lohninger** is one of the creative minds behind the project. He merges different fields of expertise to get powerful insights on how technology shapes our society. With his professional background in IT as a Senior Fellow of the Mozilla Foundation and as a cultural scientist, he brings forth innovative perspectives that are always rooted in policy discussions.

**Petra Schmidt** is a cultural anthropologist and has been a project manager for many years. She is a wizard when it comes to the creative use of machines. She was a tech columnist for specialist magazines and is highly experienced in professional data protection. Her motto is: there is a hack for everything, just be creative in finding it!

Thanks very much to **Thomas Heumesser** ([ttntm](https://ttntm.me)) for creating, supporting and maintaining our content & styling.

# Support

The Project was made possible with the financial support of Ma7 Kulturabteilung der Stadt Wien, Projektzeitraum (verlängert wg. Corona): 12/2019-12/2021